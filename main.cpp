#include <iostream>
#include <complex>

#include "CU.h"
#include "CUFFT.h"

#define NR_REPETITIONS 5

uint64_t flops_fft(
    uint64_t size,
    uint64_t batch)
{
    return 1ULL * 4 * batch * size * size * log2(size * size);
}

uint64_t bytes_fft(
    uint64_t size,
    uint64_t batch)
{
    return 1ULL * 2 * batch * size * size * 2 * sizeof(float);
}

void run_default(
    unsigned int size,
    cu::Device &device)
{
    std::clog << size << "\t";

    auto memory_total = device.get_total_memory() / ((float) 1024*1024*1024); // GBytes
    auto memory_free  = device.get_free_memory()  / ((float) 1024*1024*1024); // GBytes
    auto memory_used  = memory_total - memory_free;

    cu::Stream stream;
    uint64_t sizeof_data = 4 * size * size * sizeof(std::complex<float>);

    auto memory_data = sizeof_data / ((float) 1024*1024*1024); // GBytes
    cu::DeviceMemory d_data(sizeof_data);

    auto memory_plan = device.get_free_memory() / ((float) 1024*1024*1024); // GBytes
    cufft::C2C_2D fft_plan(size, size);
    fft_plan.setStream(stream);
    memory_plan -= device.get_free_memory() / ((float) 1024*1024*1024); // GBytes

    cu::Event start, end;

    float runtime = 0;
    for (unsigned int r = 0; r < NR_REPETITIONS; r++) {
        stream.record(start);
        for (unsigned int i = 0; i < 4; i++) {
            cufftComplex *data_ptr = reinterpret_cast<cufftComplex *>(static_cast<CUdeviceptr>(d_data));
            data_ptr += i * size * size;
            fft_plan.execute(data_ptr, data_ptr);
        }
        stream.record(end);
        stream.synchronize();

        runtime += end.elapsedTime(start);
    }

    runtime /= NR_REPETITIONS;
    runtime *= 1e-3; // seconds
    std::cout << runtime << "\t";
    std::cout << memory_data << "\t";
    std::cout << memory_plan << std::endl;
}

void run_composite(
    unsigned int size,
    cu::Device &device)
{
    std::clog << size << "\t";

    auto memory_total = device.get_total_memory() / ((float) 1024*1024*1024); // GBytes
    auto memory_free  = device.get_free_memory()  / ((float) 1024*1024*1024); // GBytes
    auto memory_used  = memory_total - memory_free;

    cu::Stream stream;
    uint64_t sizeof_data = 4 * size * size * sizeof(std::complex<float>);

    auto memory_data = sizeof_data / ((float) 1024*1024*1024); // GBytes
    if (memory_data > memory_free) {
        std::cerr << "can not allocate " << memory_data << ", available: " << memory_free << std::endl;
        return;
    }
    cu::UnifiedMemory u_data(sizeof_data, CU_MEMHOSTREGISTER_PORTABLE);

    auto memory_plan = device.get_free_memory() / ((float) 1024*1024*1024); // GBytes
    cufft::C2C_1D fft_plan_row(size, 1, 1, 1);
    cufft::C2C_1D fft_plan_col(size, size, 1, 1);
    fft_plan_row.setStream(stream);
    fft_plan_col.setStream(stream);
    memory_plan -= device.get_free_memory() / ((float) 1024*1024*1024); // GBytes

    cu::Event start, end;

    float runtime = 0;
    for (unsigned int r = 0; r < NR_REPETITIONS; r++) {
        stream.record(start);
        for (unsigned i = 0; i < 4; i++) {
            cufftComplex *data_ptr = reinterpret_cast<cufftComplex *>(u_data.ptr());
            data_ptr += i * size * size;

            // Execute 1D FFT over all rows
            for (unsigned row = 0; row < size; row++) {
                cufftComplex *ptr = data_ptr;// + row * size;
                fft_plan_col.execute(ptr, ptr);
            }

            // Execute 1D FFT over all columns
            for (unsigned col = 0; col < size; col++) {
                cufftComplex *ptr = data_ptr + col;
                fft_plan_row.execute(ptr, ptr);
            }
       }

        stream.record(end);
        stream.synchronize();

        runtime += end.elapsedTime(start);
    }

    runtime /= NR_REPETITIONS;
    runtime *= 1e-3; // seconds
    std::cout << runtime << "\t";
    std::cout << memory_data << "\t";
    std::cout << memory_plan << std::endl;
}

void run_batched(
    unsigned int size,
    cu::Device &device)
{
    std::clog << size << "\t";

    cu::Stream stream;
    const unsigned int batch = 1024;
    uint64_t sizeof_data = batch * 4 * size * size * sizeof(std::complex<float>);

    auto memory_data = sizeof_data / ((float) 1024*1024*1024); // GBytes
    cu::DeviceMemory d_data(sizeof_data);

    auto stride = 1;
    auto dist = size * size;
    auto memory_plan = device.get_free_memory() / ((float) 1024*1024*1024); // GBytes
    cufft::C2C_2D fft_plan(size, size, stride, dist, batch * 4);
    fft_plan.setStream(stream);
    memory_plan -= device.get_free_memory() / ((float) 1024*1024*1024); // GBytes

    cu::Event start, end;

    float runtime = 0;

    auto nr_repetitions = 1024;
    for (unsigned int r = 0; r < nr_repetitions; r++) {
        stream.record(start);
        cufftComplex *data_ptr = reinterpret_cast<cufftComplex *>(static_cast<CUdeviceptr>(d_data));
        fft_plan.execute(data_ptr, data_ptr);
        stream.record(end);
        stream.synchronize();
        runtime += end.elapsedTime(start);
    }

    runtime /= nr_repetitions;
    runtime *= 1e-3; // seconds
    auto flops = flops_fft(size, batch * 4) / runtime;
    auto bytes = bytes_fft(size, batch * 4) / runtime;
    bytes *= 2; // cuFFT goes through memory twice
    std::cout << runtime << "\t";
    std::cout << flops * 1e-9 << "\t";
    std::cout << bytes * 1e-9 << std::endl;
}

int main(int argc, char **argv) {
    cu::init();

    const char *device_id_str = getenv("CUDA_DEVICE");
    unsigned int device_id = device_id_str ? atoi(device_id_str) : 0;
    cu::Device device(device_id);
    cu::Context context(device);
    context.reset();

    std::clog << "# " << device.get_name() << std::endl;

    // Default mode: 2D FFT
#if 0
    // Run powers of two
    for (unsigned int size = 1024; size <= 16384; size *= 2) {
        run_default(size, device);
    } // end for size

    // Run misc large sizes
    for (int size = 1000; size <= 30000; size += 1000) {
        run_default(size, device);
    } // end for size
#endif

    // Batched 2D FFT
#if 1
    for (unsigned int size = 16; size <= 64; size += 4) {
        run_batched(size, device);
    }
#endif

    // Composite mode: 1D FFTs over columns and over rows
#if 0
    // Run powers of two
    for (unsigned int size = 1024; size <= 16384; size *= 2) {
        run_composite(size, device);
    } // end for size

    // Run misc large sizes
    for (int size = 1000; size <= 30000; size += 1000) {
        run_composite(size, device);
    } // end for size
#endif

    return EXIT_SUCCESS;
}
