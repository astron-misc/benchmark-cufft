CCX    = g++
CFLAGS = -O3 -g -fopenmp
CUDAFLAGS += -L${CUDA_ROOT}/lib64/stubs -lcuda -lcufft
CUDAFLAGS += -L${CUDA_ROOT}/lib64 -lnvToolsExt

default: main.x main-multi.x

main.x: main.cpp CU.cpp CUFFT.o
	${CCX} -o $@ $^ ${CFLAGS} ${CUDAFLAGS}

main-multi.x: main-multi.cpp CU.cpp CUFFT.o
	${CCX} -o $@ $^ ${CFLAGS} ${CUDAFLAGS}

CU%.o: CU%.cpp
	${CCX} -o $@ $^ -c ${CFLAGS} ${CUDAFLAGS}
