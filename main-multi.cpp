#include <iostream>
#include <complex>
#include <cstring>
#include <algorithm>
#include <iomanip>

#include <omp.h>

#include "CU.h"
#include "CUFFT.h"

#define NR_REPETITIONS 5

/*
 * Enable one CUDA host buffer per device, rather than having
 * only one host buffer assigned to the first device.
 */
#define USE_MULTIPLE_HOST_MEMORY 0

uint64_t flops_fft(
    uint64_t size,
    uint64_t batch)
{
    return 1ULL * 4 * batch * size * size * log2(size * size);
}

uint64_t bytes_fft(
    uint64_t size,
    uint64_t batch)
{
    return 1ULL * 2 * batch * size * size * 2 * sizeof(float);
}

std::vector<unsigned int> split_string(const char *string, const char *delimiter) {
    std::vector<unsigned int> splits;
    char * string_buffer = new char [strlen(string)+1];
    std::strcpy (string_buffer, string);
    char *token = strtok(string_buffer, delimiter);
    if (token) splits.push_back(atoi(token));
    while (token) {
        token = strtok(NULL, delimiter);
        if (token) splits.push_back(atoi(token));
    }
    delete [] string_buffer;
    return splits;
}

void run_multi(
    unsigned int size,
    std::vector<cu::Device*>& devices,
    std::vector<cu::Context*>& contexts,
    std::vector<cu::Stream*>& streams)
{
    std::clog << "# size = " << size << "\n";

    // Four FFTs need to be performed, one per polarization
    unsigned int nr_polarizations = 4;

    // Size of full dataset (one polarizations)
    uint64_t sizeof_data = size * size * sizeof(std::complex<float>);

    // Allocate resources per GPU
    std::vector<cu::DeviceMemory*> d_datas;
#if USE_MULTIPLE_HOST_MEMORY
    std::vector<cu::HostMemory*> h_datas;
#endif

    for (unsigned int i = 0; i < devices.size(); i++) {
        if (i >= nr_polarizations) continue;
        contexts[i]->setCurrent();
        cu::DeviceMemory* d_data = new cu::DeviceMemory(sizeof_data);
        d_datas.push_back(d_data);
#if USE_MULTIPLE_HOST_MEMORY
        cu::HostMemory* h_data = new cu::HostMemory(sizeof_data);
        h_datas.push_back(h_data);
#endif
    }

    // Create FFT plans
    std::vector<cufft::C2C_2D*> fft_plans;

    for (unsigned int d = 0; d < devices.size(); d++) {
        if (d >= nr_polarizations) continue;
        contexts[d]->setCurrent();
        cufft::C2C_2D* fft_plan = new cufft::C2C_2D(size, size);
        fft_plan->setStream(*streams[d]);
        fft_plans.push_back(fft_plan);
    }

    // Create events
    std::vector<cu::Event*> start_input;
    std::vector<cu::Event*> start_compute;
    std::vector<cu::Event*> end_compute;
    std::vector<cu::Event*> end_output;

    for (unsigned int r = 0; r < NR_REPETITIONS; r++) {
        for (unsigned int p = 0; p < nr_polarizations; p++) {
            unsigned int d = p % devices.size();
            contexts[d]->setCurrent();
            start_input.push_back(new cu::Event());
            start_compute.push_back(new cu::Event());
            end_compute.push_back(new cu::Event());
            end_output.push_back(new cu::Event());
        }
    }

    // Allocate data on host
#if !USE_MULTIPLE_HOST_MEMORY
    contexts[0]->setCurrent();
    cu::HostMemory* h_data = new cu::HostMemory(nr_polarizations * sizeof_data);
#endif

    // Runtime
    std::vector<float> runtime_input(nr_polarizations);
    std::vector<float> runtime_compute(nr_polarizations);
    std::vector<float> runtime_output(nr_polarizations);

    // Start timing
    for (unsigned int d = 0; d < devices.size(); d++) {
        streams[d]->synchronize();
    }
    double runtime_total = -omp_get_wtime();

    for (unsigned int r = 0; r < NR_REPETITIONS; r++) {

        // Do the work
        for (unsigned int p = 0; p < nr_polarizations; p++) {
            unsigned int d = p % devices.size();
            unsigned int e = r * nr_polarizations + p;
            contexts[d]->setCurrent();
#if USE_MULTIPLE_HOST_MEMORY
            std::complex<float> *h_ptr = (std::complex<float> *) h_datas[d]->get();
#else
            std::complex<float> *h_ptr = (std::complex<float> *) h_data->get();
            h_ptr += p * size * size;
#endif
            cufftComplex *d_ptr = reinterpret_cast<cufftComplex *>(static_cast<CUdeviceptr>(*d_datas[d]));
            streams[d]->record(*start_input[e]);
            streams[d]->memcpyHtoDAsync((CUdeviceptr) d_ptr, h_ptr, sizeof_data);
            streams[d]->record(*start_compute[e]);
            fft_plans[d]->execute(d_ptr, d_ptr);
            streams[d]->record(*end_compute[e]);
            streams[d]->memcpyDtoHAsync(h_ptr, (CUdeviceptr) d_ptr, sizeof_data);
            streams[d]->record(*end_output[e]);
        }
    }

    // End timing
    for (unsigned int d = 0; d < devices.size(); d++) {
        streams[d]->synchronize();
    }
    runtime_total += omp_get_wtime();

    // Get runtime per polarization
    for (unsigned int r = 0; r < NR_REPETITIONS; r++) {
        for (unsigned int p = 0; p < nr_polarizations; p++) {
            unsigned int d = p % devices.size();
            unsigned int e = r * nr_polarizations + p;
            contexts[d]->setCurrent();
            if (r == 0) {
                runtime_input[p]   = 0.0;
                runtime_compute[p] = 0.0;
                runtime_output[p]  = 0.0;
            }
            runtime_input[p]   += start_compute[e]->elapsedTime(*start_input[e]);
            runtime_compute[p] += end_compute[e]->elapsedTime(*start_compute[e]);
            runtime_output[p]  += end_output[e]->elapsedTime(*end_compute[e]);
        }
    }

    // Set output precision
    std::cout << std::scientific;
    std::cout << std::setprecision(2);

    // Report per polarization
    for (unsigned int p = 0; p < nr_polarizations; p++) {
        runtime_input[p]   /= NR_REPETITIONS;
        runtime_compute[p] /= NR_REPETITIONS;
        runtime_output[p]  /= NR_REPETITIONS;
        runtime_input[p]   *= 1e-3; // seconds
        runtime_compute[p] *= 1e-3; // seconds
        runtime_output[p]  *= 1e-3; // seconds
        float bw_input  = (1e-9 * sizeof_data) / runtime_input[p];  // GB/s
        float bw_output = (1e-9 * sizeof_data) / runtime_output[p]; // GB/s
        std::cout << "pol " << p << " -> ";
        std::cout << "input: "   << bw_input << " GB/s";
        std::cout << ", compute: " << runtime_compute[p] << " s";
        std::cout << ", output: "  << bw_output << " GB/s";
        std::cout << std::endl;
    }

    // Report overall runtime
    runtime_total /= NR_REPETITIONS;
    std::cout << "total: " << runtime_total << " s" << std::endl;

    // Free GPU resources
#if !USE_MULTIPLE_HOST_MEMORY
    delete h_data;
#endif
    for (unsigned int p = 0; p < nr_polarizations; p++) {
        delete start_input[p];
        delete start_compute[p];
        delete end_compute[p];
        delete end_output[p];
    }
    for (unsigned i = 0; i < devices.size(); i++) {
        delete d_datas[i];
#if USE_MULTIPLE_HOST_MEMORY
        delete h_datas[i];
#endif
    }
}

int main(int argc, char **argv) {
    cu::init();

    // Determine which GPUs to use
    const char *device_id_str = getenv("CUDA_DEVICE");
    std::vector<unsigned int> device_numbers;
    if (!device_id_str) {
        // Use device 0 if no CUDA devices were specified
        device_numbers.push_back(0);
    } else {
        device_numbers = split_string(device_id_str, ",");
    }

    // Create CUDA objects
    std::vector<cu::Device*> devices;
    std::vector<cu::Context*> contexts;
    std::vector<cu::Stream*> streams;

    for (auto device_number : device_numbers) {
        cu::Device* device = new cu::Device(device_number);
        cu::Context* context = new cu::Context(*device);
        context->setCurrent();
        cu::Stream* stream = new cu::Stream();
        devices.push_back(device);
        contexts.push_back(context);
        streams.push_back(stream);
    }

    // Print devices
    for (unsigned int i = 0; i < devices.size(); i++) {
        std::clog << "# " << i << ": " << devices[i]->get_name() << std::endl;
    }

    // Default mode: 2D FFT

    // Run powers of two
    for (unsigned int size = 1024; size <= 16384; size *= 2) {
        run_multi(size, devices, contexts, streams);
    } // end for size

    // Run misc large sizes
    //for (int size = 1000; size <= 30000; size += 1000) {
    //    run_multi(size, devices, contexts, streams);
    //} // end for size

    // Free CUDA objects
    for (unsigned int i = 0; i < devices.size(); i++) {
        delete devices[i];
        delete streams[i];
        delete contexts[i];
    }

    return EXIT_SUCCESS;
}
